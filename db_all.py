import os

import dataclasses

import numpy as np
import pandas as pd
import sqlalchemy.orm
import sqlalchemy_utils

import typing

DB_URI = (
    "postgresql://postgres:postgres@"
    f"{os.getenv('DB_TEST_HOST', 'localhost')}:5432/foobar")

DEFAULT_ENGINE = sqlalchemy.create_engine(
    DB_URI,
    isolation_level="REPEATABLE READ",
)


def ensure_database_exists(engine: sqlalchemy.engine.Engine) -> None:
    if not sqlalchemy_utils.database_exists(engine.url):
        sqlalchemy_utils.create_database(engine.url)


ensure_database_exists(DEFAULT_ENGINE)

# XXX: Maybe use expire_on_commit = False kwarg here
DEFAULT_SESSION_FACTORY = sqlalchemy.orm.sessionmaker(bind=DEFAULT_ENGINE)

mapper_registry = sqlalchemy.orm.registry()
metadata = sqlalchemy.MetaData()

dummy_revenues_table = sqlalchemy.Table(
    "dummy_revenues_table", metadata,
    sqlalchemy.Column(
        "pk", sqlalchemy.BigInteger, primary_key=True, autoincrement=True),
    sqlalchemy.Column("unit_price", sqlalchemy.Float),
    sqlalchemy.Column("no_orders", sqlalchemy.BigInteger))


@dataclasses.dataclass
class RevenuesLine:
    unit_price: float
    no_orders: int


mapper_registry.map_imperatively(RevenuesLine, dummy_revenues_table)
metadata.create_all(DEFAULT_ENGINE)


def upload_dummy_data(
        engine: sqlalchemy.engine.Engine, table_name="dummy_revenues_table"):
    np.random.seed(666)
    df = pd.DataFrame(
        {
            "unit_price": np.random.random(50),
            "no_orders": np.arange(50)})
    df.to_sql(name=table_name, con=engine, if_exists="replace")


def add_revenues_column(
        select: sqlalchemy.sql.Select,
        obj_: typing.Type[RevenuesLine]) -> sqlalchemy.sql.Select:
    col = sqlalchemy.sql.label("revenues", obj_.no_orders * obj_.unit_price)
    select = select.add_columns(col)
    return select


def main():
    ensure_database_exists(DEFAULT_ENGINE)
    upload_dummy_data(DEFAULT_ENGINE)
    select = sqlalchemy.select(
        RevenuesLine.no_orders,
        RevenuesLine.unit_price).where(RevenuesLine.no_orders > 15)
    updated_select = add_revenues_column(select, RevenuesLine)
    assert isinstance(updated_select, sqlalchemy.sql.Select)
    df = pd.read_sql_query(sql=updated_select, con=DEFAULT_ENGINE)
    assert "revenues" in df.columns
    assert len(df) > 0


if __name__ == '__main__':
    main()
