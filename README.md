# Bugreport SQLAlchemy inspection

Minimal script to reproduce https://youtrack.jetbrains.com/issue/PY-54608

* Please note that the overall stucture & usecase presented here is only 
  superficially resembling the original code and is not production grade.
* Please run `python ./db_all.py` to show that the code is working, however 
  Pycharm's code inspection seems to be off.

![Linting Problem in Pycharm](./Code_inspection_problem.png)