SHELL=/bin/sh

auto_format:
	yapf -ipr .

install_dependencies:
	pip install -r requirements.txt

install_system_dependencies:
	# might need to be run as root or with sudo
	apt-get update -y
	xargs -a ./system_requirements_debian.list apt-get install --no-install-recommends -y

update_dependencies:
	pip-compile
